<div class="fl-content-slider">
	
	<?php 
	if(empty($_ENV["slider_menu"])){
		get_template_part("includes/slider-menu");
		$_ENV["slider_menu"]=1;
	}
	
	if($settings->saleslide =="cde")
	{
		$settings->slides =[];		

	}
	if( $settings->saleslide =="yes" || $settings->saleslide =="cde")
	{
		$seleinformation = json_decode(get_option('salesliderinformation'));

		usort($seleinformation, 'compare_cde_some_objects');		

		$website_json =  json_decode(get_option('website_json'));
		$rugAffiliateCode = isset($website_json->rugAffiliateCode)?$website_json->rugAffiliateCode:"";

			$t = count($seleinformation) ;

		 foreach($settings->slides as $slides) {
			
			$slides->order = $t;

			$t++;
			
		 }	 

		if( count($seleinformation) > 0 ){		
			

			foreach($seleinformation as $slide){

				
				if($slide->isRugShop == 'true'){

					$target = "_blank";

					if($rugAffiliateCode!=''){
				  
					$slide->slide_link = 'https://rugs.shop/?store='.$rugAffiliateCode;

					}else{

						$slide->slide_link = 'https://rugs.shop/';

					}

				}else{

					$target = "_self";
				}

				$slide_coupon_img = "https://mobilem.liquifire.com/mobilem?source=url[".$slide->slider->slider_coupan_img."]&scale=height[440]&sink";
				

				$saleslide =(object) array( "label" => "Slide 1",				
				"bg_layout" => "photo",
				"bg_photo" => 394671,
				"bg_photo_src" =>  $slide->slider->slider_bg_img,
				"bg_color" => "",
				"bg_video" =>"",
				"content_layout" => "photo",
				"fg_photo" => 303556,
				"fg_photo_class" => "salebanner-slide",
				"fg_photo_src" => $slide_coupon_img,
				"fg_video" =>"",
				"title" =>"",
				"text" =>"",
				"title_tag" => "h2",
				"title_size" => "default",
				"title_custom_size" => "24",
				"text_position" => "center",
				"text_width" => "100",
				"text_margin_top" => 60,
				"text_margin_bottom" => 60,
				"text_margin_left" => 60,
				"text_margin_right" => 60,
				"text_color" => "ffffff",
				"text_shadow" => 0,
				"text_bg_color" =>"",
				"text_bg_opacity" => 70,
				"text_bg_height" => "auto",
				"link" => $slide->slide_link,
				"link_target" => $target,
				"link_nofollow" => "no",
				"cta_type" => "none",
				"cta_text" =>"",
				"btn_icon" =>"",
				"btn_icon_position" => "before",
				"btn_icon_animation" => "disable",
				"btn_bg_color" => "f7f7f7",
				"btn_bg_hover_color" =>"",
				"btn_text_color" => "333333",
				"btn_text_hover_color" =>"",
				"btn_style" => 'flat',
				"btn_border_size" => 2,
				"btn_bg_opacity" => 0,
				"btn_bg_hover_opacity" => 0,
				"btn_button_transition" => "disable",
				"btn_font_size" => 16,
				"btn_padding" => 14,
				"btn_border_radius" => 6,
				"r_photo_type" => "main",
				"r_photo" =>"",
				"r_photo_src" =>"",
				"r_text_color" => "ffffff",
				"r_text_bg_color" => "333333",
				"0" =>"",
				"link-search" =>"",
				"flrich1551177824205_text" =>"",
				"as_values_link-search" =>"",
				"flrich1551178150830_text" =>"",
				"flrich1553073775779_text" =>"",
				"flrich1553073854078_text" =>"",
				"flrich1555089382117_text" =>"",
				"flrich1555090798050_text" =>"",
				"flrich1555122396394_text" =>"",
				"flrich1555321890582_text" =>"",
				"flrich1555427799385_text" =>"",
				"flrich1555512912616_text" =>"",
				"flrich1555512937317_text" =>"",
				"order" =>$slide->slider->order,
			
			);
				
				array_push($settings->slides,$saleslide);
				
			}

			if( $settings->saleslide !="cde" ){
			
				$removed = array_shift($settings->slides);

			}
			
		}


	}
	
	
	// ...

	if($settings->saleslide == 'yes' && $settings->saleslide_order =="first"){
		
		usort($settings->slides, 'compare_some_objects');
		$settings->slides =array_reverse($settings->slides);
		
	}elseif($settings->saleslide == 'cde'){

		usort($settings->slides, 'compare_some_objects');
		$settings->slides =array_reverse($settings->slides);

	}
	
// echo '<pre>';
// 	print_r($settings->slides);
// 	echo '</pre>';
// 	exit;

	?>

	<div class="fl-content-slider-wrapper">
		<?php
	
		for($i = 0; $i < count($settings->slides); $i++) :

			if(!is_object($settings->slides[$i])) {
				continue;
			}
			else {
				$slide = $settings->slides[$i];
			}
		?>
		<div class="fl-slide fl-slide-<?php echo $i; ?> fl-slide-text-<?php echo $slide->text_position; ?>">
			<?php
			
				// Mobile photo or video
				$module->render_mobile_media($slide);

				// Background photo or video
				$module->render_background($slide);
			

			?>
			<div class="fl-slide-foreground clearfix">
				<?php
				// Content
				$module->render_content($slide);

				// Foreground photo or video
				$module->render_media($slide);
			
				?>
			</div>
		</div>
	<?php endfor; ?>
	</div>
		<?php

		// Render the navigation.
		if( $settings->arrows && count( $settings->slides ) > 0 ) : ?>
			<div class="fl-content-slider-navigation" aria-label="content slider buttons">
				<a class="slider-prev" href="#" aria-label="previous" aria-role="button"><div class="fl-content-slider-svg-container"><?php include FL_BUILDER_DIR .'img/svg/arrow-left.svg'; ?></div></a>
				<a class="slider-next" href="#" aria-label="next" aria-role="button"><div class="fl-content-slider-svg-container"><?php include FL_BUILDER_DIR .'img/svg/arrow-right.svg'; ?></div></a>
			</div>
		<?php endif; ?>
		<div class="fl-clear"></div>
</div>
